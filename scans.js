var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var scanSchema = new Schema({ //This is where scanSchema is defined.
	deviceID: String,
   	scanDate: String,
    lat: Number,
    lng: Number,
    telstraFound: Boolean
});

module.exports.scanSchema = scanSchema; //Export scanSchema so that models.js can access it.