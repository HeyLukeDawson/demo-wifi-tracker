var express = require('express'),
        app = express();

var locations = [];
var timeNowInMs = 0;
var scanTimeOffset = 0;
var actualScanTime = 0;

var request = require('request');
var bodyParser = require('body-parser');

var Pusher = require('pusher');
var pusher = new Pusher({
  appId: '230499',
  key: '6158968989055ce4c0fa',
  secret: 'aeb7c57a931d7996d76e',
  cluster: 'ap1'
});

// mongoose databsse stuff
var Models = require("./models"); //Instantiate a Models object so you can access the models.js module.

// parse application/json
app.use(bodyParser.json());
// and also parse x-www-form
app.use(bodyParser.urlencoded());

// serve up static files that are in /public folder (this will be home page)
app.use(express.static('public'));


// GET a dump of the whole DB of wifi locations
app.get('/allrecords', function(req, res) {
  	// read in the database
  	Models.Scan.find(function(err, scansplease) {
	  	if (err)
	  		return console.error(err);
	  	else
	 		res.send(scansplease);
 	});
});


// GET a particular device's last location
app.get('/:id/lastlocation', function(req, res) {
    var q = Models.Scan.find({"deviceID":req.params.id}).sort({"_id": -1}).limit(1);
	q.exec(function(err, location) {
		if (err)
	  		return console.error(err);
	  	else 
	 		res.send(location);
	});
});


// GET the last 100 locations for a given device ID
app.get('/:id/last100locations', function(req, res) {
	var q = Models.Scan.find({"deviceID":req.params.id}).sort({"_id": -1}).limit(100);
	//var q = Models.Scan.find({"_id":{ "$gte": "579846aa87771be800eb662f", "$lte": "5798551d81241b7d12b05f23"}});
	q.exec(function(err, locations) {
		if (err)
	  		return console.error(err);
	  	else 
	 		res.send(locations);
	});
});


// GET the distinct deviceIDs that are present in the database
app.get('/deviceIDs', function(req, res) {
	Models.Scan.find().distinct('deviceID', function(err, IDs) {
		if (err)
	  		return console.error(err);
	  	else 
			res.send(IDs);
	});
});


// SigFox POST received
app.post('/api/sigfox', function(req,res) {

	console.log(req.body.packet);	
	res.send(200, "ok, McGarnagle");

	var firstString = req.body.packet.substr(0,2) + ":"
					+ req.body.packet.substr(2,2) + ":"
					+ req.body.packet.substr(4,2) + ":"
					+ req.body.packet.substr(6,2) + ":"
					+ req.body.packet.substr(8,2) + ":"
					+ req.body.packet.substr(10,2);
	var secondString = req.body.packet.substr(12,2) + ":"
					+ req.body.packet.substr(14,2) + ":"
					+ req.body.packet.substr(16,2) + ":"
					+ req.body.packet.substr(18,2) + ":"
					+ req.body.packet.substr(20,2) + ":"
					+ req.body.packet.substr(22,2);	
	var googs = {
		"considerIP":"false",
		"wifiAccessPoints":[
			{"macAddress":firstString},
			{"macAddress":secondString}
		]
	};
	console.log(firstString);
	console.log(secondString);
	console.log(googs);



	// get the request ready and then send to the googs
	request({
	    url: "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyA4TYVZGEW7j_jmqdETGukUKYAZxXKY0VI", //URL to hit
	    method: "POST",
    	json: googs
	}, function(error, response, body){
	    if(error) {
	        console.log(error);
	    } else {
	    	// Google has sent us a response...
	        console.log(response.statusCode, body);

			// fire off the pusher event to the browser
			pusher.trigger('test_channel', 'my_event', {"message": "new map point received", "lat":body.location.lat, "lng":body.location.lng});
		}
	});
});


// Received a POST to /api/scans which we will then send to google geolocate
app.post('/api/scans', function(req, res) {

	// store the mac address of the device
	var deviceID = req.body.macAddress;

	// check if this is a current sample or a saved sample
	if ( !req.body.hasOwnProperty('savedScan') ) {
		// then this was the most recent scan, so save the millisecond value
		timeNowInMs = Date.now();
		scanTimeOffset = timeNowInMs - req.body.milliStamp;
		actualScanTime = timeNowInMs;
	}
	// we received a saved scan, so we should have only got here if we have a successful current scan, so the scanTimeOffset should be set properly
	else {
		actualScanTime = req.body.milliStamp + scanTimeOffset;
	}

	// check if there was a "Telstra Air" hotspot found in this scan
	var telstraFound = false;
	if (req.body.hasOwnProperty('foundTelstra'))
		telstraFound = true;

	// ESP8266 has already formatted the POST JSON data, so let's just send straight on through to the googs
	request({
	    url: "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyA4TYVZGEW7j_jmqdETGukUKYAZxXKY0VI", //URL to hit
	    method: "POST",
    	json: req.body.scanData
	}, function(error, response, body){
	    if(error) {
	        console.log(error);
	    } else {
	    	// Google has sent us a response...
	        console.log(response.statusCode, body);

	        // flick the resonse back to the ESP request 
	        var d = new Date(actualScanTime);
			res.send(res.statusCode, 'Date: ' + d.toLocaleString('en-GB', {timeZone: 'Australia/Sydney'}) + ', {lat: ' + body.location.lat + ', lng: ' + body.location.lng + '}');	

			var newScanReceived = new Models.Scan({
				deviceID: deviceID,
			   	scanDate: actualScanTime, //d.toLocaleString('en-GB', {timeZone: 'Australia/Sydney'}),
			    lat: body.location.lat,
			    lng: body.location.lng,
			    telstraFound: telstraFound
			});

			newScanReceived.save(function(error) {
			   console.log("Your bee has been saved.");
				if (error) 
			   		console.error(error);
			});	

			// fire off the pusher event to the browser
			pusher.trigger('test_channel', 'my_event', {"message": "new map point received", "lat":body.location.lat, "lng":body.location.lng});
		}
	});

});

// Received a POST to /api/ble which is a hardcoded demo for now...
app.post('/api/ble', function(req, res) {

	res.send(200, "ok BLE");

	// fire off the pusher event to the browser
	pusher.trigger('test_channel', 'ble_event', {"message": "new map point received", "lat":-33.869046, "lng":151.207384});
});


// start app
app.listen('8080');
console.log('App started on port 8080');