#include "ESP8266WiFi.h"

// all the globals
String scanData = "";
int numberOfStoredScans = 0;
const int MAXSCANS = 30;
String scans[MAXSCANS];
const char *network = "Telstra Air";
const char *password = "";
bool WIFI_CONNECTED = false;
WiFiClient client;

void setup() {
    Serial.begin(115200);
    delay(1000);
    
    // Set WiFi to station mode and disconnect from an AP if it was previously connected
    WiFi.mode(WIFI_STA);    
    Serial.println("\n\nSetup wrapped.");

    //ESP.deepSleep(5000); 
}

void loop() {

    if (!ScanForNetworks()){
        delay(5000);
        return;
    }
    
    // Scan complete, now try and join WiFi network
    if (!TryConnectToTA()) {
        
        // Couldn't connect, so store the scan results
        if (numberOfStoredScans < MAXSCANS) {
            scanData.setCharAt(scanData.length()-1, ',');
            scanData += "\"savedScan\":true}";
            scans[numberOfStoredScans] = scanData;
            numberOfStoredScans++;
        }
        delay (5000);
        return;
    }

    // We are connected to WiFi, so let's POST off the data to Dawson server  *** Change to API.IOTCORES.COM when it is ready to accept
    if (!client.connect("wifitracking-83904.onmodulus.net", 80)) {
        Serial.println("Connection to host failed.");
                
        // Couldn't connect, so store the scan results
        if (numberOfStoredScans < MAXSCANS) {
            scanData.setCharAt(scanData.length()-1, ',');
            scanData += "\"savedScan\":true}";
            scans[numberOfStoredScans] = scanData;
            numberOfStoredScans++;
        }
        return;
    }

    // Send the current scan result
    if (!PostScanResult(scanData)){

        // failed to post scan data to server
        Serial.println("Failed to POST off the data :(");

        // so lets flag this sample to be saved and store it
        if (numberOfStoredScans < MAXSCANS) {
            scanData.setCharAt(scanData.length()-1, ',');
            scanData += "\"savedScan\":true}";
            scans[numberOfStoredScans] = scanData;
            numberOfStoredScans++;
        }
        return;
    }

    // are there any saved scans to send?
    if (numberOfStoredScans) {

        // lets send them off to the server one at a time
        for (int i=0; i<numberOfStoredScans; i++) {
            if (!PostScanResult(scans[i]))
                return;
        }
        // so we sent them all off, reset the storage please
        numberOfStoredScans = 0;
    }

    // Let's scan again! But wait a bit first...
    delay(5000);
}


bool ScanForNetworks() {
    Serial.println("\n\n ******** Starting WiFi scan ********");
    
    // WiFi.scanNetworks will return the number of networks found
    bool FOUND_TELSTRA = false;
    int n = WiFi.scanNetworks();
    //Serial.println("Scan complete.");
    if (n == 0) {
        Serial.println("No networks found"); 
        return false;
    } else if (n == 1) {
        Serial.println("Only 1 network found"); 
        return false;
    } else {
        //Serial.print(n);
        //Serial.println(" networks found");

        // save macAddress - use this for deviceID
        byte mac[6], macbyte;
        WiFi.macAddress(mac);
        String macString = "";
        for ( int b = 0; b < 6; b++ ) {
            macbyte = mac[b];
            if (macbyte < 15)
                macString += "0";           
            macString += String(macbyte, HEX);
        }
        scanData = "{\"macAddress\":\"";
        scanData += macString;
        scanData += "\",\"scanData\":{\"considerIP\":\"false\","
         "\"wifiAccessPoints\":[";
    
        for (int i = 0; i < n; ++i) {
            scanData += "{\"macAddress\":";
      
            byte bssid[6]; 
            byte bb; 
            uint8_t *point; 
            point = WiFi.BSSID(i);
            
            String bssidString = "\"";
            for ( int b = 0; b < 6; b++ ) {
                bb = *(point + b);
                if (bb < 15)
                    bssidString += "0";           
                bssidString += String(bb, HEX);
                if (b != 5)
                    bssidString += ":";  
            }
            scanData += bssidString;
    
            scanData += "\",\"signalStrength\":";
            scanData += String(WiFi.RSSI(i)) + "}";
            if (i != n-1)
                scanData += ",";

            // finally, let's check if any of the networks were 'Telstra Air'
            if (WiFi.SSID(i) == "Telstra Air")
                FOUND_TELSTRA = true;
              
        }
        scanData += "]},\"milliStamp\":";
        scanData += String(millis()) + "}";

        if (FOUND_TELSTRA) {
            scanData.setCharAt(scanData.length()-1, ',');
            scanData += "\"foundTelstra\":true}";
        }
            

        return true;
    }
}

bool TryConnectToTA() {

    // are we already connected?
    if (WiFi.status() == WL_CONNECTED)
        return true;
        
    Serial.println();
    Serial.print("connecting to ");
    Serial.print(network);
    WiFi.begin(network, password);
    int counter = 0;
    while (WiFi.status() != WL_CONNECTED) {
        delay(300);
        Serial.print(".");
        counter++;
        if (counter > 20) {
            WIFI_CONNECTED = false;
            Serial.println("Couldn't connect to TA");
            return false;
        }
    }
    Serial.println("");
    Serial.println("Connected to TA");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    WIFI_CONNECTED = true;
    return true;
}

bool PostScanResult(String scan){
    // This will send the request to the server
    String postRequest =
        String("POST ") + "/api/scans" + " HTTP/1.1\r\n" +
        "Host: " + "wifitracking-83904.onmodulus.net" + "\r\n" +
        "Accept: *" + "/" + "*\r\n" +
        "Content-Length: " + scan.length() + "\r\n" +
        "Content-Type: application/json\r\n" +
        "\r\n" + scan;
    //Serial.println("Sending off results to Dawson server (and then to Google Geolocate API)");
    //Serial.println(postRequest);
    client.print(postRequest);
    unsigned long timeout = millis();
    while (client.available() == 0) {
        if (millis() - timeout > 15000) {
            Serial.println("Request timed out :(");
            client.stop();
            
            return false;
        }
    }
    //Serial.println("...request sent.  Response is:");
    //Serial.println("________________________________");
    String line = "";
    while (client.available()) {
        line = client.readStringUntil('\r');
        if (line.indexOf("lat: ") != -1)
            Serial.print(line);
    }
    //Serial.println();
    //Serial.println("________________________________");

    return true;
}
