# README #

LX legends.

To get this up and running you will need to read up about node.js if you haven't already.  Also some basic knowledge on how to drive npm would be helpful.

This project was created with Modulus.io in mind for hosting, so there are a few variables specific to a modulus environment, as well as the Mongo DB which is hosted in Modulus.

If you need help give me a buzz of course :)